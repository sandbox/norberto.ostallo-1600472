<?php

/**
 * @file
 * Doc Version Views integration
 *
 * @ingroup print
 */

/**
 * Implements hook_views_data().
 */
function print_doc_views_data() {
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['print_doc_node_conf']['table']['group'] = t('Printer-friendly version');
  $data['print_doc_page_counter']['table']['group'] = t('Printer-friendly version');

  // This table references the {node} table. The declaration below creates an
  // 'implicit' relationship to the node table, so that when 'node' is the base
  // table, the fields are automatically available.
  $data['print_doc_node_conf']['table']['join']['node'] = array(
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'left_field' => 'nid',
    'field' => 'nid',
//    'type' => 'INNER',
  );
  $data['print_doc_page_counter']['table']['join']['node'] = array(
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'left_field' => 'nid',
    'field' => 'path',
//    'type' => 'INNER',
    'handler' => 'print_doc_join_page_counter',
  );

  // print_doc_node_conf fields
  $data['print_doc_node_conf']['link'] = array(
    'title' => t('Doc: Show link'),
    'help' => t('Whether to show the Doc version link.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['print_doc_node_conf']['comments'] = array(
    'title' => t('Doc: Show link in individual comments'),
    'help' => t('Whether to show the Doc version link in individual comments.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['print_doc_node_conf']['url_list'] = array(
    'title' => t('Doc: Show Printer-friendly URLs list'),
    'help' => t('Whether to show the URL list.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['print_doc_node_conf']['size'] = array(
    'title' => t('Doc: Paper size'),
    'help' => t('Configured Doc paper size'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['print_doc_node_conf']['orientation'] = array(
    'title' => t('Doc: Page orientation'),
    'help' => t('Configured Doc page orientation.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  // print_doc_page_counter fields
  $data['print_doc_page_counter']['totalcount'] = array(
    'title' => t('Doc: Number of page accesses'),
    'help' => t('Counter of accesses to the Doc version for this node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['print_doc_page_counter']['timestamp'] = array(
    'title' => t('Doc: Last access'),
    'help' => t("The date of the last access to the node's Doc version."),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
