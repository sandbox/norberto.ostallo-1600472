<?php

/**
 * @file
 * Generates the Doc version using html header
 *
 * This file is included by the print_doc_htmlheader module
 *
 * @ingroup print
 */

/**
 * Implements hook_print_doc_generate().
 */
function print_doc_htmlheader_print_doc_generate($html, $meta, $paper_size = NULL, $page_orientation = NULL) {
global $base_url, $language;

  module_load_include('inc', 'print', 'includes/print');

  $doc_tool = explode('|', variable_get('print_doc_doc_tool', PRINT_DOC_DOC_TOOL_DEFAULT));
  if (empty($paper_size)) {
    $paper_size = variable_get('print_doc_paper_size', PRINT_DOC_PAPER_SIZE_DEFAULT);
  }
  if (empty($page_orientation)) {
    $page_orientation = variable_get('print_doc_page_orientation', PRINT_DOC_PAGE_ORIENTATION_DEFAULT);
  }
  $content_disposition = variable_get('print_pdf_content_disposition', PRINT_PDF_CONTENT_DISPOSITION_DEFAULT);
  $images_via_file = variable_get('print_pdf_images_via_file', PRINT_PDF_IMAGES_VIA_FILE_DEFAULT);

  // Try to use local file access for image files
  $html = _print_access_images_via_file($html, $images_via_file);

  // Spaces in img URLs must be replaced with %20
  $pattern = '!<(img\s[^>]*?)>!is';
  $html = preg_replace_callback($pattern, '_print_replace_spaces', $html);

  $url_array = parse_url($meta['url']);

  $protocol = $url_array['scheme'] . '://';
  $host = $url_array['host'];
  $path = dirname($url_array['path']) . '/';

  if ($filename) {
    if (headers_sent()) {
      die("Unable to stream Doc: headers already sent");
    }
    header("Cache-Control: private");
    header("Content-Type', 'application/ms-word; utf-8");
    $attachment =  ($content_disposition == 2) ?  "attachment" :  "inline";
    header("Content-Disposition: $attachment; filename=\"$filename\"");
    echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">';
    echo $html;
    flush();
    return TRUE;
  }
  else {
    return $html;
  }
}
