<?php

/**
 * @file
 * Contains the administrative functions of the Doc version module.
 *
 * This file is included by the Doc version module, and includes the
 * settings form.
 *
 * @ingroup print
 */

/**
 * Form constructor for the Doc version module settings form.
 *
 * @ingroup forms
 */
function print_doc_settings() {
  $doc_tools = array();
  drupal_alter('print_doc_available_libs', $doc_tools);

  if (!empty($doc_tools)) {
    $link = print_doc_print_link();

    $current_doc_tool = variable_get('print_doc_doc_tool', PRINT_DOC_DOC_TOOL_DEFAULT);
    $doc_tool_default =  array_key_exists($current_doc_tool, $doc_tools) ? $current_doc_tool : PRINT_DOC_DOC_TOOL_DEFAULT;

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Doc options'),
    );

    $form['settings']['print_doc_doc_tool'] = array(
      '#type' => 'radios',
      '#title' => t('Doc generation tool'),
      '#options' => $doc_tools,
      '#default_value' => $doc_tool_default,
      '#description' => t('This option selects the Doc generation tool being used by this module to create the Doc version.'),
    );

    $form['settings']['print_doc_content_disposition'] = array(
      '#type' => 'radios',
      '#title' => t('Open Doc in'),
      '#options' => array(t('Same browser window'), t('New browser window'), t('Save dialog')),
      '#default_value' => variable_get('print_doc_content_disposition', PRINT_DOC_CONTENT_DISPOSITION_DEFAULT),
      '#description' => t("Select the desired method for opening the Doc in the user's browser."),
    );

    $form['settings']['print_doc_paper_size'] = array(
      '#type' => 'select',
      '#title' => t('Paper size'),
      '#options' => _print_doc_paper_sizes(),
      '#default_value' => variable_get('print_doc_paper_size', PRINT_DOC_PAPER_SIZE_DEFAULT),
      '#description' => t('Choose the paper size of the generated Doc.'),
    );

    $form['settings']['print_doc_page_orientation'] = array(
      '#type' => 'select',
      '#title' => t('Page orientation'),
      '#options' => array('portrait' => t('Portrait'), 'landscape' => t('Landscape')),
      '#default_value' => variable_get('print_doc_page_orientation', PRINT_DOC_PAGE_ORIENTATION_DEFAULT),
      '#description' => t('Choose the page orientation of the generated Doc.'),
    );

    $form['settings']['print_doc_cache_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable caching of generated Docs'),
      '#default_value' => variable_get('print_doc_cache_enabled', PRINT_DOC_CACHE_ENABLED_DEFAULT),
    );

    $form['settings']['print_doc_cache_lifetime'] = array(
      '#type' => 'select',
      '#title' => t('Cache Lifetime'),
      '#options' => array(
        '0'      => 'None',
        '10800'  => '3 hours',
        '21600'  => '6 hours',
        '43200'  => '12 hours',
        '64800'  => '18 hours',
        '86400'  => '24 hours',
        '129600' => '36 hours',
        '172800' => '2 days',
        '259200' => '3 days',
        '345600' => '4 days',
        '432000' => '5 days',
        '518400' => '6 days',
        '604800' => '7 days',
      ),
      '#default_value' => variable_get('print_doc_cache_lifetime', PRINT_DOC_CACHE_LIFETIME_DEFAULT),
      '#description' => t('The lifetime of cached Docs. A cached Doc is only removed when a node is updated, deleted, or cron is run and the last access is older than this value.'),
    );

    $form['settings']['print_doc_images_via_file'] = array(
      '#type' => 'checkbox',
      '#title' => t('Access images via local file access'),
      '#default_value' => variable_get('print_doc_images_via_file', PRINT_DOC_IMAGES_VIA_FILE_DEFAULT),
      '#description' => t("Enabling this option will make the tool use local file access for image files. This option is not recommended to use in conjunction with modules like imagecache which generate the image after it's first accessed. However, it may be necessary in low-end hosting services where the web server is not allowed to open URLs and the user can't modify that configuration setting."),
    );
    $form['settings']['print_doc_autoconfig'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto-configure the Doc tool settings'),
      '#default_value' => variable_get('print_doc_autoconfig', PRINT_DOC_AUTOCONFIG_DEFAULT),
      '#description' => t('If you disable this option, the doc tool settings must be configured manually.'),
    );

    $form['settings']['print_doc_filename'] = array(
      '#type' => 'textfield',
      '#title' => t('Doc filename'),
      '#default_value' => variable_get('print_doc_filename', PRINT_DOC_FILENAME_DEFAULT),
      '#description' => t("If left empty the generated filename defaults to the node's path. Tokens may be used to build the filename (see following list). The .doc extension will be appended automatically."),
    );
    if (module_exists('token')) {
      $form['settings']['print_doc_filename_patterns'] = array(
        '#type' => 'fieldset',
        '#title' => t('Replacement patterns'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['settings']['print_doc_filename_patterns']['descriptions'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('node'),
      );
    }

    $form['settings']['print_doc_display_sys_urllist'] = array(
      '#type' => 'checkbox',
      '#title' => t('Printer-friendly URLs list in system pages'),
      '#default_value' => variable_get('print_doc_display_sys_urllist', PRINT_TYPE_SYS_URLLIST_DEFAULT),
      '#description' => t('Enabling this option will display a list of printer-friendly destination URLs at the bottom of the page.'),
    );

    $form['settings']['link_text'] = array(
      '#type' => 'fieldset',
      '#title' => t('Custom link text'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['settings']['link_text']['print_doc_link_text_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable custom link text'),
      '#default_value' => variable_get('print_doc_link_text_enabled', PRINT_TYPE_LINK_TEXT_ENABLED_DEFAULT),
    );

    $form['settings']['link_text']['print_doc_link_text'] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('print_doc_link_text', $link['text']),
      '#description' => t('Text used in the link to the Doc version.'),
    );

    $form['#validate'][] = '_print_doc_settings_validate';
  }
  else {
    variable_set('print_doc_doc_tool', PRINT_DOC_DOC_TOOL_DEFAULT);

    $form['settings'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t("No Doc generation tool found! Please download and/or enable a supported Doc generation tool. Check this module's INSTALL.txt for more details.") . '</p>',
    );
  }

  return system_settings_form($form);
}

/**
 * Form validation handler for print_doc_settings().
 *
 * @see print_doc_settings()
 * @ingroup forms
 */
function _print_doc_settings_validate($form, &$form_state) {
  if (empty($form_state['values']['print_doc_doc_tool'])) {
    form_set_error('print_doc_doc_tool', t("No Doc tool selected"));
  }
}
