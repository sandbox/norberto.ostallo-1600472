<?php

/**
 * @file
 * Hooks provided by the Doc version module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide some information on the needs of the Doc library.
 *
 * @return
 *   Associative array with the following data:
 *   - name: name of the Doc library.
 *   - min_version: minimum version of the Doc library supported by the
 *     module.
 *   - url: URL where the Doc library can be downloaded from.
 *   - expand_css: boolean flag indicating whether to expand the CSS files
 *     in the HTML passed to the Doc library, or to leave it as a list of
 *     include directives.
 *   - public_dirs: directories to which the tool requires write-access,
 *     with configurable locations.
 *   - tool_dirs: directories to which the tool requires write-access, but
 *     can't be configured, and are relative to the tool's root path.
 *
 * @ingroup print_hooks
 */
function hook_doc_tool_info() {
  return array(
    'name' => 'foodoc',
    'min_version' => '1.0',
    'url' => 'http://www.doc.tool/download',
    'expand_css' => FALSE,
    'public_dirs' => array(
      'fonts',
      'cache',
      'tmp',
    ),
    'tool_dirs' => array(
      'xyz',
    ),
  );
}

/**
 * Find out the version of the Doc library
 *
 * @param string $doc_tool
 *   Filename of the tool to be analysed.
 *
 * @return string
 *   version number of the library
 */
function hook_doc_tool_version() {
  require_once(DRUPAL_ROOT . '/' . $doc_tool);

  return '1.0';
}

/**
 * Generate a PDF version of the provided HTML.
 *
 * @param string $html
 *   HTML content of the Doc
 * @param array $meta
 *   Meta information to be used in the Doc
 *   - url: original URL
 *   - name: author's name
 *   - title: Page title
 *   - node: node object
 * @param string $paper_size
 *   (optional) Paper size of the generated Doc
 * @param string $page_orientation
 *   (optional) Page orientation of the generated Doc
 *
 * @return
 *   generated Doc page, or NULL in case of error
 *
 * @see print_doc_controller_html()
 * @ingroup print_hooks
 */
function hook_print_doc_generate($html, $meta, $paper_size = NULL, $page_orientation = NULL) {
  $doc = new Doc();
  $doc->writeHTML($html);

  return $doc->Output();
}

/**
 * Alters the list of available Doc libraries.
 *
 * During the configuration of the Doc library to be used, the module needs
 * to discover and display the available libraries. This function should use
 * the internal _print_scan_libs() function which will scan both the module
 * and the libraries directory in search of the unique file pattern that can
 * be used to identify the library location.
 *
 * @param array $doc_tools
 *   An associative array using as key the format 'module|path', and as value
 *   a string describing the discovered library, where:
 *   - module: the machine name of the module that handles this library.
 *   - path: the path where the library is installed, relative to DRUPAL_ROOT.
 *     If the recommended path is used, it begins with sites/all/libraries.
 *   As a recommendation, the value should contain in parantheses the path
 *   where the library was found, to allow the user to distinguish between
 *   multiple install paths of the same library version.
 *
 * @ingroup print_hooks
 */
function hook_print_doc_available_libs_alter(&$doc_tools) {
  module_load_include('inc', 'print', 'includes/print');
  $tools = _print_scan_libs('foo', '!^foo.php$!');

  foreach ($tools as $tool) {
    $doc_tools['print_doc_foo|' . $tool] = 'foo (' . dirname($tool) . ')';
  }
}

/**
 * Alters the Doc filename.
 *
 * Changes the value of the Doc filename variable, just before it is used to
 * create the file. When altering the variable, do not suffix it with the
 * '.doc' extension, as the module will do that automatically.
 *
 * @param string $doc_filename
 *   current value of the doc_filename variable, after processing tokens and
 *   any transliteration steps.
 * @param string $path
 *   original alias/system path of the page being converted to Doc.
 *
 * @ingroup print_hooks
 */
function hook_print_doc_filename_alter(&$doc_filename, &$path) {
  $doc_filename = 'foo';
}

/**
 * @} End of "addtogroup hooks".
 */
