<?php

/**
 * @file
 * Generates the Doc versions of the pages
 *
 * This file is included by the print_doc module and includes the
 * functions that interface with the Doc generation packages.
 *
 * @ingroup print
 */

module_load_include('inc', 'print', 'print.pages');

/**
 * Generate a Doc version of the printer-friendly page
 *
 * @see print_controller()
 * @see _print_doc_phpword()
 */
function print_doc_controller() {
  // Disable caching for generated Docs, as Drupal doesn't ouput the proper headers from the cache
  $GLOBALS['conf']['cache'] = FALSE;

  $args = func_get_args();
  $path = filter_xss(implode('/', $args));
  $cid = isset($_GET['comment']) ? (int)$_GET['comment'] : NULL;

  // Handle the query
  $query = $_GET;
  unset($query['q']);

  if (!empty($path)) {
    if ($alias = drupal_lookup_path('source', $path)) {
      // Alias
      $path_arr = explode('/', $alias);
      $node = node_load($path_arr[1]);
    }
    elseif (ctype_digit($args[0])) {
      // normal nid
      $node = node_load($args[0]);
    }

    $doc_filename = variable_get('print_doc_filename', PRINT_DOC_FILENAME_DEFAULT);
    if (!empty($doc_filename) && !empty($node)) {
      $doc_filename = token_replace($doc_filename, array('node' => $node));
    }
    else {
      $doc_filename = token_replace($doc_filename, array('site'));
      if (empty($doc_filename) || count(token_scan($doc_filename))) {
        // If there are still tokens, use a fallback solution
        $doc_filename = str_replace('/', '_', $path);
      }
    }
  }
  else {
    $doc_filename = 'page';
  }

  if (function_exists('transliteration_clean_filename')) {
    $doc_filename = transliteration_clean_filename($doc_filename, language_default('language'));
  }

  drupal_alter('print_doc_filename', $doc_filename, $path);

  $doc = print_doc_generate_path($path, $query, $cid, $doc_filename . '.doc');
  if ($doc == NULL) {
    drupal_goto($path);
    exit;
  }

  $nodepath = (isset($node->nid)) ? 'node/' . $node->nid : drupal_get_normal_path($path);
  db_merge('print_doc_page_counter')
    ->key(array('path' => $nodepath))
    ->fields(array(
        'totalcount' => 1,
        'timestamp' => REQUEST_TIME,
    ))
    ->expression('totalcount', 'totalcount + 1')
    ->execute();

  drupal_exit();
}

/**
 * Gennerate a Doc for a given Drupal path.
 *
 * @param string $path
 *   path of the page to convert to Doc
 * @param array $query
 *   (optional) array of key/value pairs as used in the url() function for the
 *   query
 * @param int $cid
 *   (optional) comment ID of the comment to render.
 * @param string $doc_filename
 *   (optional) filename of the generated Doc
 * @param string $view_mode
 *   (optional) view mode to be used when rendering the content
 *
 * @return
 *   generated Doc page, or NULL in case of error
 *
 * @see print_doc_controller()
 */
function print_doc_generate_path($path, $query = NULL, $cid = NULL, $doc_filename = NULL, $view_mode = PRINT_VIEW_MODE) {
  global $base_url;

  $link = print_doc_print_link();
  $node = print_controller($path, $link['format'], $cid, $view_mode);
  if ($node) {
    // Call the current tool's hook_doc_tool_info(), to see if we need to expand CSS
    $doc_tool = explode('|', variable_get('print_doc_doc_tool', PRINT_DOC_DOC_TOOL_DEFAULT));
    $cache_enabled = variable_get('print_doc_cache_enabled', PRINT_DOC_CACHE_ENABLED_DEFAULT);

    $function = $doc_tool[0] . '_doc_tool_info';
    if (function_exists($function)) {
      $info = $function();
    }
    $expand = isset($info['expand_css']) ? $info['expand_css'] : FALSE;

    $html = theme('print', array('node' => $node, 'query' => $query, $expand, 'format' => $link['format']));

    // Img elements must be set to absolute
    $pattern = '!<(img\s[^>]*?)>!is';
    $html = preg_replace_callback($pattern, '_print_rewrite_urls', $html);

    // Convert the a href elements, to make sure no relative links remain
    $pattern = '!<(a\s[^>]*?)>!is';
    $html = preg_replace_callback($pattern, '_print_rewrite_urls', $html);
    // And make anchor links relative again, to permit in-Doc navigation
    $html = preg_replace("!${base_url}/" . $link['path'] . '/.*?#!', '#', $html);

    $meta = array(
      'node' => $node,
      'url' => url(drupal_get_path_alias(empty($node->nid) ? $node->path : "node/$node->nid"), array('absolute' => TRUE)),
    );
    if (isset($node->name)) $meta['name'] = $node->name;
    if (isset($node->title)) $meta['title'] = $node->title;

    $paper_size = isset($node->print_doc_size) ? $node->print_doc_size : NULL;
    $page_orientation = isset($node->print_doc_orientation) ? $node->print_doc_orientation : NULL;

    $doc = '';
    $cachemiss = FALSE;
    if ($cache_enabled && isset($node->nid)) {
      // See if the file exists in the cache
      $cachefile = drupal_realpath(print_doc_cache_dir()) . '/' . $node->nid . '.doc';
      if (is_readable($cachefile)) {
        // Get the Doc content from the cached file
        $doc = file_get_contents($cachefile);
        if ($doc === FALSE) {
          watchdog('print_doc', 'Failed to read from cached file %file', array('%file' => $cached), WATCHDOG_ERROR);
        }
      }
      else {
        $cachemiss = TRUE;
      }
    }

    // If cache is off or file is not cached, generate one from scratch
    if (empty($doc)) {
      $doc = print_doc_generate_html($html, $meta, NULL, $paper_size, $page_orientation);
    }

    if (!empty($doc)) {
      // A Doc was created, save it to cache if configured
      if ($cachemiss) {
        if (file_unmanaged_save_data($doc, $cachefile, FILE_EXISTS_REPLACE) == FALSE) {
          watchdog('print_doc', 'Failed to write to "%f".', array('%f' => $filename), WATCHDOG_ERROR);
        }
      }

      return $doc_filename ? print_doc_dispose_content($doc, $doc_filename) : $doc;
    }
  }
  else {
    return NULL;
  }
}
